import matplotlib.pyplot as mpl
import numpy as num
import pandas as pan
import sklearn.discriminant_analysis as sk
import sklearn.naive_bayes as sc
import random
from sklearn.model_selection import cross_validate
import sklearn.metrics as  metrics

def get_accuracy(y, y_var):
    tmp = 0
    for x in range(y.size):
        if y[x] == y_var[x] : tmp+=1
    val = (tmp/y.size)*100
    return val

#Wczytanie danych
data = pan.read_csv('C:\\Users\\PYTON\\Desktop\\git_repo\\lsed\\zad2\\wine.data', sep=',', header=None, engine='python')
data.columns = ["clas", "Alcohol", "Malic acid", "Ash","Alcalinity of ash","Magnesium","Total phenols","Flavanoids","Nonflavanoid phenols","Proanthocyanins","Color intensity","Hue","OD280/OD315 of diluted wines","Proline"]
#data.columns = ["clas","a","b","c",'d','e','f','g',"h","i","j","k","l","m"]
X = data.iloc[:, 1:14].values
y = data.iloc[:, 0].values

X2 = data.iloc[:, 1:2].values
X5 = data.iloc[:, 1:5].values
X10 = data.iloc[:, 1:10].values

#Inicjowanie klasyfikatorów
lda = sk.LinearDiscriminantAnalysis()
lda.fit(X,y)

qda = sk.QuadraticDiscriminantAnalysis()
qda.fit(X,y)

gnb = sc.GaussianNB()
gnb.fit(X,y)

#Początkowa klasyfikacja
y_lda_test = lda.predict(X)
y_qda_test = qda.predict(X)
y_gnb_test = gnb.predict(X)

#Prametry klasyfikatorów (confusion matrices)
print("Initial confusion matrices:")
print("LDA: \n",metrics.confusion_matrix(y, y_lda_test))
print("QDA: \n",metrics.confusion_matrix(y, y_qda_test))
print("GNB: \n",metrics.confusion_matrix(y, y_gnb_test))

#Klasyfikacja dla różnych ilości zmiennych w dataFrame
lda.fit(X2,y)
qda.fit(X2,y)
gnb.fit(X2,y)
y2_lda = lda.predict(X2)
y2_qda = qda.predict(X2)
y2_gnb = gnb.predict(X2)

#print(metrics.accuracy_score(y,y2_lda))
#print(get_accuracy(y, y2_lda))
full_lda = metrics.accuracy_score(y,y2_lda)
full_qda = metrics.accuracy_score(y,y2_qda)
full_gnb = metrics.accuracy_score(y,y2_gnb)

lda.fit(X5,y)
qda.fit(X5,y)
gnb.fit(X5,y)
y5_lda = lda.predict(X5)
y5_qda = qda.predict(X5)
y5_gnb = gnb.predict(X5)

lda.fit(X10,y)
qda.fit(X10,y)
gnb.fit(X10,y)
y10_lda = lda.predict(X10)
y10_qda = qda.predict(X10)
y10_gnb = gnb.predict(X10)

print("\nAcc. for 2/5/10 number of params")
print("LDA (2):",get_accuracy(y, y2_lda),"\t QDA (2): ",get_accuracy(y, y2_qda),"\t GNB (2):",get_accuracy(y, y2_gnb))
print("LDA (5):",get_accuracy(y, y5_lda),"\t QDA (5): ",get_accuracy(y, y5_qda),"\t GNB (5):",get_accuracy(y, y5_gnb))
print("LDA (10):",get_accuracy(y, y10_lda),"\t QDA (10): ",get_accuracy(y, y10_qda),"\t GNB (10):",get_accuracy(y, y10_gnb),"\n")

#Podział na próbki
data = data.sample(frac=1) #przemieszanie danych
half = int(data.shape[0]/2)
quarter = int(data.shape[0]/4)
tmp = half+quarter
PU = data.iloc[:half, 1:3].values
PW = data.iloc[half:tmp, 1:3].values
PT = data.iloc[tmp:, 1:3].values

y_PU = data.iloc[:half, 0].values
y_PW = data.iloc[half:tmp, 0].values
y_PT = data.iloc[tmp:, 0].values

#PU -> "uczymy" na tej próbce klasyfikatory
lda.fit(PU,y_PU)
qda.fit(PU,y_PU)
gnb.fit(PU,y_PU)

#PW -> próbka walidacyjna do wybrania najlepszego klasyfikatora
y_lda_pw = lda.predict(PW)
y_qda_pw = qda.predict(PW)
y_gnb_pw = gnb.predict(PW)

acc_lda = get_accuracy(y_PW, y_lda_pw)
acc_qda = get_accuracy(y_PW, y_qda_pw)
acc_gnb = get_accuracy(y_PW, y_gnb_pw)

acc_matrix = num.array([acc_lda, acc_qda, acc_gnb])

#PT -> próbka testowa do wyznaczania faktycznej skuteczności klasyfikatora
y_lda_pt = lda.predict(PT)
y_qda_pt = qda.predict(PT)
y_gnb_pt = gnb.predict(PT)

#Wypisanie acc. dla pełnej próbki
print("LDA Acc. for full sample:",full_lda)

#Wypisanie acc. dla najlepszego (wybranego wyżej) modelu 
if acc_matrix.argmax() == 0:
    print("LDA acc. for PT:", metrics.accuracy_score(y_PT, y_lda_pt))

elif acc_matrix.argmax() == 1:
    print("QDA acc. for PT:", metrics.accuracy_score(y_PT, y_qda_pt))
else:
    print("GaussNB acc. for PT:", metrics.accuracy_score(y_PT, y_gnb_pt))

#cross-validation
cv_results = cross_validate(lda, X2, y, cv=3)
print("Test score LDA after cv:",cv_results['test_score'])

