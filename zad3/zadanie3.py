# Wykreslic skutecznosc klasyfikatora dla knn = 1 ... 21
# To samo dla TP, TN
# Wylosowac 10 pkt z 1 klasy i 5 z klasy 2, potraktowac jako zbior testowy i powtorzyc wykresy.

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as num
import pandas as pan
import math
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix

#zadane parametry
#ilosci
n1 = 30
n2 = 20
sum = n1 + n2
#macierze kowariancji
S1 = num.array([[4,2],[2,4]])
S2 = num.array([[4,2],[2,2]])
#średnie
mean1 = num.array([-1,-1])
mean2 = num.array([2,2])
#klasy
Class1 = num.random.multivariate_normal(mean1, S1, n1)
Class2 = num.random.multivariate_normal(mean2, S2, n2)

#X - Zestaw wszystkich danych
#y - Zestaw przynalezności punktów do jednej z 2 klas
X = num.concatenate((Class1,Class2))
y = num.zeros(sum)
y[n1:] = 1

#do pozniejszego losowania
y1 = num.zeros(n1)
y2 = num.ones(n2)
'''
#test dla n = 5
classifier = KNeighborsClassifier(n_neighbors=5)
classifier.fit(X, y)
y_pred = classifier.predict(X)

print(confusion_matrix(y, y_pred))
'''
#inicjalizacja tablic trzymajacych wartosci
scores = num.zeros(21)
TP = num.zeros(21)
TN = num.zeros(21)
#petla dla różnej liczby k_nn
for n_neighbours in range(21):
	classifier = KNeighborsClassifier(n_neighbours+1) #nowy klasyfikator dla danego k_nn
	classifier.fit(X, y) 
	y_pred = classifier.predict(X) #tablica przewidywanych przynaleznosci do klas
	confusionM = confusion_matrix(y, y_pred)
	#wzięcie skutecznosci dla klasyfikacji oraz TN i TP
	scores[n_neighbours] = classifier.score(X,y)
	TP[n_neighbours] = confusionM[0][0]
	TN[n_neighbours] = confusionM[1][1]

#Podział na próbkę testową i treningową
Class1_train, Class1_test, y1_train, y1_test = train_test_split(Class1, y1, test_size=10)
Class2_train, Class2_test, y2_train, y2_test = train_test_split(Class2, y2, test_size=5)

X_train = num.concatenate((Class1_train, Class2_train))
X_test = num.concatenate((Class1_test, Class2_test))
y_train = num.concatenate((y1_train, y2_train))
y_test = num.concatenate((y1_test, y2_test))

#Ta sama procedura co poprzednio (dla nie podzielonych danych)
scores2 = num.zeros(21)
TP2 = num.zeros(21)
TN2 = num.zeros(21)
for n_neighbours in range(21):
	classifier = KNeighborsClassifier(n_neighbours+1)
	classifier.fit(X_train, y_train)
	y_pred2 = classifier.predict(X_test)
	confusionM = confusion_matrix(y, y_pred)
	scores2[n_neighbours] = classifier.score(X,y)
	TP2[n_neighbours] = confusionM[0][0]
	TN2[n_neighbours] = confusionM[1][1]


#Plotowanie wyników
canvas = plt.figure(figsize=(10,15))

plot1 = canvas.add_subplot(2,3,1)
plot1.scatter(Class1[:,0], Class1[:,1], c="blue",label="class 1")
plot1.scatter(Class2[:,0], Class2[:,1], c="red", label="class 2")
plot1.title.set_text("Points")
plot1.set_xlabel("x")
plot1.set_ylabel("y")
plot1.legend()

plot2 = canvas.add_subplot(2,3,2)
plot2.scatter(num.arange(1,22), scores, c="blue")
plot2.title.set_text("score dist.")
plot2.set_xlabel("k_nn")

plot3 = canvas.add_subplot(2,3,3)
plot3.scatter(num.arange(1,22), TP, c="green", label="TP val.")
plot3.scatter(num.arange(1,22), TN, c="red", label="TN val.")
plot3.title.set_text("TP & TN values")
plot3.set_xlabel("k_nn")
plot3.legend()

plot4 = canvas.add_subplot(2,3,4)
plot4.scatter(Class1_train[:,0], Class1_train[:,1], c="blue",label="class 1 train")
plot4.scatter(Class1_test[:,0], Class1_test[:,1], c="lightblue",label="class 1 test")
plot4.scatter(Class2_train[:,0], Class2_train[:,1], c="red", label="class 2 train")
plot4.scatter(Class2_test[:,0], Class2_test[:,1], c="tomato", label="class 2 test")
plot4.title.set_text("Points")
plot4.set_xlabel("x")
plot4.set_ylabel("y")
plot4.legend()

plot5 = canvas.add_subplot(2,3,5)
plot5.scatter(num.arange(1,22), scores2, c="blue")
plot5.title.set_text("score dist.")
plot5.set_xlabel("k_nn")

plot6 = canvas.add_subplot(2,3,6)
plot6.scatter(num.arange(1,22), TP2, c="green", label="TP val.")
plot6.scatter(num.arange(1,22), TN2, c="red", label="TN val.")
plot6.title.set_text("TP & TN values")
plot6.set_xlabel("k_nn")
plot6.legend()

plt.show()