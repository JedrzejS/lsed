import matplotlib.pyplot as mpl
import numpy as num
import pandas as pan
import sklearn.naive_bayes as sc

def project_on_line(X,A):
    Xz = (X[1] * A + X[0]) / (A**2 + 1) #X[0] -> x'owa, X[1] -> y'owa
    Yz = A * Xz
    return num.array([Xz,Yz])

m1 = num.array([-1,1])
m2 = num.array([2,4])
m3 = num.array([-2,2])
S = num.array([[1,0],[0,1]])

Class1 = num.random.multivariate_normal(m1,S,30)
Class2 = num.random.multivariate_normal(m2,S,30)
Class3 = num.random.multivariate_normal(m3,S,30)

df1 = pan.DataFrame(Class1)
df2 = pan.DataFrame(Class2)
df3 = pan.DataFrame(Class3)

df_all = df1 + df2 + df3

mpl.scatter(Class1[:,0],Class1[:,1], label = 'class 1')
mpl.scatter(Class2[:,0],Class2[:,1], label = 'class 2')
mpl.scatter(Class3[:,0],Class3[:,1], label = 'class 3')

mean1 = num.average(df1)
mean2 = num.average(df2)
mean3 = num.average(df3)

cov1 = num.cov(df1)
cov2 = num.cov(df2)
cov3 = num.cov(df3)

n1 = num.ma.size(df1,1)
n2 = num.ma.size(df2,1)
n3 = num.ma.size(df3,1)

n = n1+n2+n3
g = 3

mean_all = num.average(df_all)

tmp_t1 = num.transpose(mean1 - mean_all)
tmp_t2 = num.transpose(mean2 - mean_all)
tmp_t3 = num.transpose(mean3 - mean_all)

tmpB1 = num.dot(n1*(mean1 - mean_all),tmp_t1)
tmpB2 = num.dot(n2*(mean2 - mean_all),tmp_t2)
tmpB3 = num.dot(n3*(mean3 - mean_all),tmp_t3)

B = (tmpB1 + tmpB2 + tmpB3)/(g-1)

W = 1/(n - g)*((n1 - 1) * cov1 + (n2 - 1) * cov2 + (n3 - 1) * cov3)

U1_pom = num.linalg.inv(W)
U = num.dot(U1_pom,B)

lamb, vect = num.linalg.eig(U)

a = vect[:,num.argmax(lamb)]

x = num.linspace(-7,7,100)
y = x*(a[1]/a[0])

mpl.plot(x,y)

A = a[1]/a[0]

proj1 = project_on_line(df1,A)
proj2 = project_on_line(df2,A)
proj3 = project_on_line(df3,A)

mpl.scatter(proj1[0,:],proj1[1,:], c = 'blue', marker = 'x')
mpl.scatter(proj2[0,:],proj2[1,:], c = 'orange', marker = 'x')
mpl.scatter(proj3[0,:],proj3[1,:], c = 'green', marker = 'x')

mpl.ylim(-7,7)
mpl.xlim(-7,7)

mpl.legend()

mpl.show()