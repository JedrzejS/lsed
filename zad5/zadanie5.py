import matplotlib.pyplot as mpl
import numpy as num
import pandas as pan
import sklearn.discriminant_analysis as sk
import sklearn.naive_bayes as sc
import random
from sklearn.model_selection import cross_validate
import sklearn.metrics as  metrics
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn.metrics import confusion_matrix
from sklearn.ensemble import BaggingClassifier

iris = datasets.load_iris()
X = iris.data
y = iris.target

X_train, X_test, y_train,  y_test = train_test_split(X, y, train_size=0.8)

tree_bagging_wrongs = num.zeros(6)
i = 0
lda_bagging_wrongs = num.zeros(6)
j = 0
#Baggowanie drzew
for n in [1,2,5,10,20,50]:
    clf_tree = tree.DecisionTreeClassifier(random_state=None)
    #clf_tree.fit(X_train, y_train)
    tree_bag = BaggingClassifier(base_estimator = clf_tree, n_estimators = n)
    tree_bag.fit(X_train, y_train)
    tree_bag_confM = confusion_matrix(y_test, tree_bag.predict(X_test))
    
    #bierzemy wszystko poza diagonalą i dodajemy = mamy liczbe blednych klasyfikacji
    tree_bagging_wrongs[i] = tree_bag_confM[0][1] + tree_bag_confM[0][2] + tree_bag_confM[1][0] + tree_bag_confM[1][2] + tree_bag_confM[2][0] + tree_bag_confM[2][1]
    i = i + 1

#Baggowanie LDA
for n in [1,2,5,10,20,50]:
    clf_lda = LinearDiscriminantAnalysis()
    lda_bag = BaggingClassifier(base_estimator = clf_lda, n_estimators = n)
    lda_bag.fit(X_train, y_train)
    lda_bag_confM = confusion_matrix(y_test, lda_bag.predict(X_test))
    
    #bierzemy wszystko poza diagonalą i dodajemy = mamy liczbe blednych klasyfikacji
    lda_bagging_wrongs[j] = lda_bag_confM[0][1] + lda_bag_confM[0][2] + lda_bag_confM[1][0] + lda_bag_confM[1][2] + lda_bag_confM[2][0] + lda_bag_confM[2][1]
    j = j + 1

#Bagging za dużo nie zmienia... Za mała próbka danych?
print("Bledy dla drzew decyzyjnych (1,2,5,10,20,50): ", tree_bagging_wrongs)
print("Bledy dla lda (1,2,5,10,20,50): ", lda_bagging_wrongs)