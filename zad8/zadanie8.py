import numpy as num
import pandas as pan
import matplotlib as mpl 
import matplotlib.pyplot as plt
import scipy as sp 
from sklearn.decomposition import PCA

#Wczytanie danych
data = pan.read_csv('C:\\Users\\PYTON\\Desktop\\git_repo\\lsed\\zad2\\wine.data', sep=',', header=None, engine='python')
data.columns = ["class", "Alcohol", "Malic acid", "Ash","Alcalinity of ash","Magnesium","Total phenols","Flavanoids","Nonflavanoid phenols","Proanthocyanins","Color intensity","Hue","OD280/OD315 of diluted wines","Proline"]
X = data.iloc[:,1:14]
y = data['class']

#PCA
pca = PCA(n_components = len(X.columns))
pca.fit(data)

#wyliczanie skumulowanej wariancji
cumulative_variance = num.cumsum(pca.explained_variance_ratio_)
print(cumulative_variance)

#wizualizacja skumulowanej wariancji
plt.scatter(range(1, 14), cumulative_variance)
plt.xlabel('number of components')
plt.ylim((0.998, 1.0005))
plt.title('cumulative variance (%)')
plt.show()

#wyliczanie nowych zmiennych i transformacja zbioru
after_pca = pca.transform(data)
#print(after_pca)

#wykres w nowych zmiennych dla 1,2
plt.scatter(after_pca[y == 1][:, 0], after_pca[y == 1][:, 1], label='c1')
plt.scatter(after_pca[y == 2][:, 0], after_pca[y == 2][:, 1], label='c2') 
plt.scatter(after_pca[y == 3][:, 0], after_pca[y == 3][:, 1], label='c3')
plt.title("New var distribution (1, 2)")
plt.xlabel("component 1")
plt.ylabel("component 2")
plt.legend()
plt.show()

#wykres w nowych zmiennych dla 2,3
plt.scatter(after_pca[y == 1][:, 1], after_pca[y == 1][:, 2], label='c1')
plt.scatter(after_pca[y == 2][:, 1], after_pca[y == 2][:, 2], label='c2') 
plt.scatter(after_pca[y == 3][:, 1], after_pca[y == 3][:, 2], label='c3')
plt.title("New var distribution (2, 3)")
plt.xlabel("component 2")
plt.ylabel("component 3")
plt.legend()
plt.show()