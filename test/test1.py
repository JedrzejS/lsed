import matplotlib.pyplot as mpl
import numpy as num
import pandas as pan

arr1 = num.array([[2,2,2,1,3], [2,1,3,2,2]])
arr2 = num.array([[6,6,6,5,7], [0,1,-1,0,0]])
df1 = pan.DataFrame(arr1)
df2 = pan.DataFrame(arr2)

av1 = num.average(df1)
av2 = num.average(df2)

cov1 = num.cov(df1)
cov2 = num.cov(df2)

n1 = num.ma.size(df1,1)
n2 = num.ma.size(df2,1)
n = n1 + n2

W = ((n1-1)*cov1+(n2-1)*cov2/(n-2))

A = num.transpose(W).dot(av2-av1)

mpl.scatter([2,2,2,1,3], [2,2,2,1,3])
mpl.show()
print("tab1: \n",df1)
print("tab2: \n",df2)
print("average1: ", av1)
print("average2: ",av2)
print("cov1: \n",cov1)
print("cov2: \n",cov2)
print("nrow1: ",n1," nrow2: ",n2)

