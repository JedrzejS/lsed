import pandas as pan
import numpy as num
import matplotlib.pyplot as plt
from sklearn.svm import SVC
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import cross_val_score

#Generacja danych (Gauss 2D)
S = num.array([[4, 2],[2, 4]])
n1 = 30 
n2 = 20 
m1 = num.array([-1,-1]) 
m2 = num.array([2,2])
class0 = num.random.multivariate_normal(m1, S, n1) 
class1 = num.random.multivariate_normal(m2, S, n2)

X = num.concatenate((class0, class1))
y = num.zeros(n1+n2)
y[n1:] = 1

#Wyrysowanie danych
plt.scatter(class0[:,0], class0[:,1], label = "class0")
plt.scatter(class1[:,0], class1[:,1], label = "class1")
plt.legend()
plt.show()

#Wektory podpierające
C_count = 10

#inicjalizacja macierzy parametrów prostych
a_c = num.zeros(C_count)
b_c = num.zeros(C_count)

#macierz parametrów
C = num.zeros(C_count)
for i in range (1,C_count+1):
    C[i - 1] = 0.1*i
#print(C)

#macierz wyników
svm_clf_score = num.zeros(C_count)
svm_clf_err = num.zeros(C_count)

i = 0
#klasyfikacja svc
for c in C:
    clf_svc = SVC(C = c)
    clf_svc.fit(X, y)
    clf_cv = cross_val_score(clf_svc, X, y, cv=10)
    svm_clf_score[i] = num.mean(clf_cv)
    svm_clf_err[i] = num.std(clf_cv)
    i = i + 1

# lda
lda = LinearDiscriminantAnalysis()
lda.fit(X, y)
lda_cv = cross_val_score(lda, X, y, cv=10)
lda_score = num.mean(lda_cv)
lda_err = num.std(lda_cv)

#rzeczy do wykresu
lda_score_plot = num.zeros(C_count)
lda_err_plot = num.zeros(C_count)
j = 0
for i in range (1, 11):
    lda_score_plot[j] = lda_score
    lda_err_plot[j] = lda_err
    j = j + 1

#porównanie wyników dla różnych C

### TODO: dodac do scatter error-bar'y
fig = plt.figure()
ax1 = plt.subplot() 
ax1 = fig.add_subplot(1,1,1)
ax1.scatter(C, svm_clf_score, label = "svm")
ax1.scatter(C, lda_score_plot, label = "lda")
ax1.legend()
ax1.set_title("Porównanie wyników klasyfikacji svm vs. lda")
ax1.set_xlabel('C')
ax1.set_ylabel("Acc.")
plt.show()